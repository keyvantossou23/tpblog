<?php

use Router\Router;

require '../vendor/autoload.php';




define('VIEWS', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR);



$router = new  Router($_GET['url']);

$router->get('/', 'App\Controllers\BlogController@welcom');

$router->get('/posts', 'App\Controllers\BlogController@index');

$router->get('/posts/:id', 'App\Controllers\BlogController@show');

$router->get('/categories', 'App\Controllers\BlogController@cat');

$router->get('/listcat', 'App\Controllers\BlogController@listcat');

$router->get('/del/:id', 'App\Controllers\BlogController@del');

$router->post('/updatecat', 'App\Controllers\BlogController@update');

$router->post('/cate', 'App\Controllers\BlogController@cate');

$router->run();
