<?php

namespace App\Models;

use Database\DBConnection;

class Model
{

    protected $db;
    protected $table;

    public function __construct(DBConnection $db)
    {
        $this->db = $db;
    }

    public function all(): array
    {
        $stmt = $this->db->getPDO()->query("SELECT * FROM {$this->table}");

        return $stmt->fetchAll();
    }

    public function findById(int $id)
    {
        $stmt = $this->db->getPDO()->prepare("SELECT * FROM {$this->table} WHERE id = ? ");

        $stmt->execute([$id]);
        return $stmt->fetch();
    }

    public function delete(int $id)
    {
        $stmt = $this->db->getPDO()->prepare("DELETE FROM {$this->table} WHERE id = ? ");
        // $stmt->execute([$id]);
        // return $stmt->fetch();

        if ($stmt->execute([$id])) {
            header('location:' . $_SERVER['HTTP_REFERER']);
        } else {
            return false;
        }
    }

    // public function change(string $libelle, int $id)
    // {
    //     $stmt = $this->db->getPDO()->prepare("UPDATE {$this->table} SET libelle = ? WHERE id = ? ");
    //     if ($stmt->execute(array($libelle, $id))) {
    //         header('location:' . $_SERVER['HTTP_REFERER']);
    //     } else {
    //         return false;
    //     }
    // }





    // public function insertCat(string $libelle)
    // {
    //     $libelle = $_POST['libelle'];
    //     $id = $_POST['id'];
    //     $stmt = $this->db->getPDO()->prepare("INSERT INTO categorie VALUE(?,?);");
    //     if ($stmt->execute([$id, $libelle])) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // public function allcats(): array
    // {
    //     $stmt = $this->db->getPDO()->query("SELECT * FROM {$this->table} ORDER BY created_at DESC");

    //     return $stmt->fetchAll();
    // }


}
