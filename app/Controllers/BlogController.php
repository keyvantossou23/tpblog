<?php

namespace App\Controllers;

use App\Models\Categorie;
use App\Models\Post;


class BlogController extends Controller
{
    public function welcom()
    {
        return $this->view('Blog.welcom');
    }



    public function index()
    {

        $post = new Post($this->getDB());

        $posts = $post->all();


        return $this->view('Blog.index', compact('posts'));
    }



    public function show(int $id)
    {
        $post = new Post($this->getDB());

        $post = $post->findById($id);


        return $this->view('Blog.show', compact('post'));
    }

    public function cat()
    {
        return $this->view('Blog.cat');
    }

    // public function insertCat(string $libelle)
    // {
    //     $post = new Post($this->getDB());

    //     $post = $post->insertCat($libelle);


    //     return $this->view('Blog.insertCat');
    // }

    public function cate()
    {
        return $this->view('Blog.cate');
    }

    public function update(){
        return $this->view('Blog.updatecat');
    

    }
       




    public function listcat()
    {

        $post = new Categorie($this->getDB());

        $all = $post->all();


        return $this->view('Blog.listcat', compact('all'));
    }

    public function del(int $id)
    {
        $post = new Categorie($this->getDB());

        $del = $post->delete($id);
        
    }

    // public function update(string $libelle, int $id)
    // {
    //     $post = new Categorie($this->getDB());

    //     $del = $post->change($libelle, $id);
        
    // }
}
