<div style="background-color:#444; opacity:0.7; height: 300px; background-image:url(../../public/images/slider1.jpg); background-size:cover;">
    <div style="text-align: center; padding-top:100px">
        <h1 style="color:black; font-weight:bold; font-size:60px">Les articles</h1>
    </div>
</div>


<div class="modal" id="updatemodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="mb-3">Modifier une catégorie</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/updatecat" method="post">
                    <div class="mb-3">
                        <label class="form-label">id</label>
                        <input type="text" class="form-control" id="updateid" name="updateid" >
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Libelle</label>
                        <input type="text" class="form-control" id="updatelibelle" name="updatelibelle">
                    </div>
                    

                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" name="updatedata" class="btn btn-primary w-100">Valider</button>
               
            </div>
        </div>
    </div>
</div>



<!-- 
<div class="album py-5 bg-light">
    <div class="container">

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3"> -->

<table class="table table-bordered table-light">
    <thead>
        <th scope="col">ID</th>
        <th scope="col">Libelle</th>
        <th scope="col">Action</th>
    </thead>


    <?php


    foreach ($params['all'] as $categories) :  ?>

        <!-- <div class="col">
            <div class="card">
                <div class="card-body">
                    <h3 style="color:#444"> </h3>
                    <small></small>
                    <a href="/del/" class="btn btn-danger"> Delete</a>
                    <a href="/update/" class="btn btn-secondary"> Update</a>
                </div>
            </div>
        </div> -->

        <!-- <div class="col mb-3 tr">
                    <div class="card shadow-sm">
                        <div class="card-header" style="text-align: center;">
                            <strong>Categorie N° </strong>
                        </div>
                        <div class="card-body">
                            <div style="text-align: center;" class="mb-2">
                                <h3 class="card-text"></h3>

                            </div>

                            <div class="d-flex justify-content-around align-items-center">
                                <div class="btn-group" style="padding:auto;">
                                    <div>
                                        <a href="/del/" class="btn btn-danger"> Delete</a>
                                    </div>
                                    <div class="ml-3">
                                        <button class="btn btn-secondary updatebtn" type="button" data-bs-toggle='modal' data-bs-target='#exampleModal'> Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->



        <tbody>
            <tr>
                <td><?= $categories->id ?></td>
                <td><?= $categories->libelle ?></td>
                <td>
                    <button class="btn btn-secondary updatebtn" type="button" data-bs-toggle='modal' data-bs-target='#updatemodal'>Update</button>

                </td>
            </tr>



        </tbody>






    <?php endforeach ?>

</table>

<!-- </div> -->
<!-- </div>
    </div>
</div> -->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script>
    $(document).ready(function() {
        $('.updatebtn').on('click', function() {
            $('#updatemodal').modal('show');

            $tr = $(this).closest('tr');
            var data = $tr.children("td").map(function() {
                return $(this).text();

            }).get();

            console.log(data);

            $('#updateid').val(data[0]);
            $('#updatelibelle').val(data[1]);


        });
    });
</script>