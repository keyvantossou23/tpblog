<div class="container-fluid">
    <div class="row mt-2">
        <div class="col-6">
            <div class="container">
                <h4 class="mb-3">Ajouter une catégorie</h4>
                <form id="form_add" action="/cate"  method="post">
                    <div class="mb-3">
                        <label class="form-label">id</label>
                        <input type="text" class="form-control" name="id" id="id">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Libelle</label>
                        <input type="text" class="form-control" name="libelle" id="libelle">
                    </div>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-primary w-100" id="ajouter">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>