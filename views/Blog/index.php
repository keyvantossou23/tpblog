<div style="background-color:#444; opacity:0.7; height: 300px; background-image:url(../../public/images/slider1.jpg); background-size:cover;">
    <div style="text-align: center; padding-top:100px">
        <h1 style="color:black; font-weight:bold; font-size:60px">Les articles</h1>
    </div>
</div>






<?php
foreach ($params['posts'] as $posts) :  ?>

    <div class="card mb-3 col-md">
        <div class="card-body">
            <h2 style="color:#444"> <?= $posts->title ?></h2>
            <small><?= $posts->created_at ?></small>
            <p><?= $posts->content ?></p>
            <a href="/posts/<?= $posts->id ?>" class="btn btn-primary"> Voir article</a>
        </div>



    </div>

<?php endforeach ?>