<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>


    <link rel="stylesheet" href=" <?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'app.js' ?>">
    <link rel="stylesheet" href=" <?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'bootstrap.min.js' ?>">
    <link rel="stylesheet" href=" <?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'app.css' ?>">
    <link rel="stylesheet" href=" <?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'style.css' ?>">






    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">



</head>

<body>

    <nav class="navbar navbar-expand-lg bg-body-tertiary fixed-top" id="nav_style">
        <div class="container">
            <a class="navbar-brand" href="#" style="font-weight: bold;"><span style="color:orange">My</span><span style="color:#6DACFF">Blog</span></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/posts">Les articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/listcat">Les catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/categories">Ajouter article</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>

                        <ul class="dropdown-menu"  aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Disabled</a>
                    </li>

                </ul>

            </div>
        </div>
    </nav>






    <style>
        #nav_style {
            background-color: #458;
        }

        .divCarrousel {
            width: 800px;
            height: 400px;
        }

        a {
            text-decoration: none;
            color: white;

        }

        a:hover {
            color: #444;
        }
    </style>







    <div>
        <?= $content ?>

    </div>



</body>

</html>